# Oh my god, this import list gives me flashbacks to java :(
# FIXME: Reduce needed imports or split some of the classes into diffrent files

# GTK import
import gi
from gi.repository import Gtk
# For thumbnails
from yt_core import helper
from gi.repository import GLib
from gi.repository import GdkPixbuf
import tempfile, requests, os
# Threading
import threading, time

# Idea: Every (Media)Item should spawn a diffrent thread

# Parent of VideoItem, PlaylistItem, ChannelItem
# gtk stuff
@Gtk.Template(filename='data/ItemMedia.ui')
class ItemMedia(Gtk.Box):

    __gtype_name__ = "ItemMedia"
    
    _title = Gtk.Template.Child()
    _thumbnail = Gtk.Template.Child()
    _owner = Gtk.Template.Child()
    _metadata = Gtk.Template.Child()

    parent = None

    def __init__(self, data, cache, size_group, parent):
        super().__init__()
        self.parent = parent
        self.data = data
        self.cache = cache
        self.set_title(data["title"])
        self.set_metadata(self.metadata())
        self.set_owner(data["ownerText"])
        self.thumbnailFilePath =  os.path.join(GLib.get_user_cache_dir(), data["Id"] + ".jpg")
        size_group.add_widget(self._thumbnail)

    # Yey, inconsistent naming style.
    # I like setTitle way more.
    # The following are like Gtk, as this is a PyGObject.
    def set_title(self, string):
        self._title.props.label = string
    def set_metadata(self, string):
        self._metadata.props.label = string
    def set_owner(self, string):
        self._owner.props.label = string
    
    thumbnailDownloaded = False
    thumbnailDownloading = False
    threadActive = False
    
    thumbnailShown = False

    def act(self):
        if self.is_visible_to_user():
            self.thumbnailThreadInit()
    
    def is_visible_to_user(self):
        self_size = self.get_allocation()
        parent_size = self.parent.get_allocation()
        out, *args = self_size.intersect(parent_size)
        return out


    def thumbnailThreadInit(self):
        if not self.threadActive:
            self.threadActive = True
            thread = self.cache.append_queue_thumbnail(self.thumbnailThread)

    def thumbnailThread(self):
        if not self.thumbnailDownloaded and not self.thumbnailDownloading:
            self.thumbnailDownloading = True
            self.initThumbnail()
            self.thumbnailDownloaded = True
            self.thumbnailDownloading = False
        # 0.25 * width looks nice, perhaps even less
        if self.thumbnailDownloaded:
            width = int(self.get_allocated_width() / 4)
            if width < 50:
                width = 50
            thumbnailPixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(self.thumbnailFilePath, width, -1)
            self._thumbnail.set_from_pixbuf(thumbnailPixbuf)
            self.thumbnailShown = True
        self.threadActive = False

    def initThumbnail(self):
        if not self.thumbnailFilePath in self.cache.files:
            # Not known file by cache? We remove it.
            if os.path.isfile(self.thumbnailFilePath):
                os.remove(self.thumbnailFilePath)
            with open(self.thumbnailFilePath, mode="w+b") as file:
                dl = self.getThumbnail()
                # FIXME: check for valid file
                file.write(dl.content)
            self.cache.add_file(self.thumbnailFilePath)
    
    def getThumbnail(self):
        request = requests.get(helper.get_worst_thumbnail_from_list(self.data["thumbnail"]))
        return request

    def metadata(self):
        def text(str1, str2):
            if str1:
                return str1 + " - " + str2
            else : return str2
        
        out = ""
        lengthText = self.data["lengthText"]
        viewCountText = self.data["viewCountText"]        
        publishedTimeText = self.data["publishedTimeText"]
        out = text(out, lengthText)
        out = text(out, viewCountText)
        return text(out, publishedTimeText)

